<?php
session_start();

  if (!isset($_SESSION["login"]) ) {
    header("Location:login.php");
    exit;
  }

require 'konek.php';
$sql = "SELECT `namaTanam`,`warna_merah`,`warna_hijau`,`warna_biru`, `tanggal` FROM `tbl_status` ORDER BY `tanggal` DESC LIMIT 10"; 
    $result = $conn->query($sql);
    $kategori = array();
    $warna_merah = array();
    $warna_hijau = array();
    $warna_biru = array();
    $tanggal = array();

    while($rc = mysqli_fetch_assoc($result)){
      array_unshift($kategori,array("label"=>$rc["tanggal"]));
      array_unshift($warna_merah,array("value"=>$rc["warna_merah"]));
      array_unshift($warna_hijau,array("value"=>$rc["warna_hijau"]));
      array_unshift($warna_biru,array("value"=>$rc["warna_biru"]));
    }

    $kategori = json_encode($kategori);
    $warna_merah = json_encode($warna_merah);
    $warna_hijau = json_encode($warna_hijau);
    $warna_biru = json_encode($warna_biru);
    ?>
<!doctype html>
<html lang="en">
  <head>

      <script type="text/javascript" src="script.js"></script>

      <!--<script type="text/javascript" src="script_fussion.js"></script>
      <script type="text/javascript" src="pustaka_FC/js/fusioncharts.js"></script>
      <script type="text/javascript" src="pustaka_FC/js/themes/fusioncharts.theme.fint.js"></script>-->
      <script src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
      <script src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
      <script type="text/javascript">
      FusionCharts.ready(function() {
            var visitChart = new FusionCharts({
              type: 'msline',
              renderAt: 'chart-container',
              width: '900',
              height: '400',
              dataFormat: 'json',
              dataSource: {
                "chart": {"theme": "fusion","caption": "data Grafik Pengujian Tanaman","xAxisName": "Value"},
                "categories": [{"category": <?php echo $kategori; ?>}],
                "dataset": [
                {"seriesname": "Warna_merah","data": <?php echo $warna_merah; ?>},
                {"seriesname": "Warna_hijau","data": <?php echo $warna_hijau; ?>},
                {"seriesname": "Warna_biru","data": <?php echo $warna_biru; ?>},
                ],
                "trendlines": [{
                  "line": [{
                    "color": "#62B58F",
                    "valueOnRight": "1"
                  }]
                }]
              }
            }).render();
          });
      </script>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

   <link rel="stylesheet" href="style.css">

    <title>Pengaturan</title>


  </head>
  <body>


<!-- navbar -->
     <section id="navbar">
          <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
          <div class="container">
        <a class="navbar-brand" href="index.php">PCTH</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
           <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#about">Tentang
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Content
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="crud.php">Buat Data</a>
                <a class="dropdown-item" href="pengaturan.php">Pengaturan</a>
                <a class="dropdown-item" href="grafik.php">Grafik</a>
                <a class="dropdown-item" href="waktu.php">Waktu Panen</a>
                <a class="dropdown-item" href="download.php">Unduh</a>
              </div>
            </li>
          </ul>
        </div>
       </div>
      </nav>
      </section>
  <!-- navbar -->



<section id="layout" class="layout">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb text-right">
        <li class="breadcrumb-item"><a href="index.php">Beranda</a></li>
        <li class="breadcrumb-item active" aria-current="page">Grafik</li>
      </ol>
    </nav>
    <div class="container">
      <h1 class="display-4">Pengaturan Cahaya Tanaman Hidroponik</h1>
      <p class="lead">Penerapan Cahaya LED pada Tanaman Hidroponik sebagai Pengganti Cahaya Matahari..</p>
    </div>
  </div> 
</div>
</section>


<!-- <section class="grafik" id="grafik">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">Hasil Grafik PCTH</div>
            <div class="panel-body"><iframe src="linecharts.php" width="100%" height="400%"></iframe> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
 -->
   <center><section class="white" id="section4">
          <div id="chart-container">FusionCharts will render here</div>
          <br/>
    </section></center>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>